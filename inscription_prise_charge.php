<!--//Cette page php est celle qui se charge des vérifications et tests du formulaire d'inscription-->

<?php
// define variables and set to empty values
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["nom"])) {
    $nomErr = "Nom attendu";
  } else {
    $nom = test_input($_POST["nom"]);
    // Vérifie si le nom contient unique des lettres et des espacesc
    if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
      $nomErr = "Entrer uniquement des lettres ou des espaces"; 
    }
  }
    
  if (empty($_POST["prenom"])) {
    $prenomErr = "Prenom attendu";
  } else {
    $prenom = test_input($_POST["prenom"]);
     // Vérifie si le nom contient unique des lettres et des espacesc
     if (!preg_match("/^[a-zA-Z ]*$/",$prenom)) {
      $prenomErr = "Entrer uniquement des lettres ou des espaces"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email attendu";
  } else {
    $email = test_input($_POST["email"]);
    // on vérifie que l'email entré a un format valide
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Format d'email invalide"; 
    }
     
  }
    
  if (empty($_POST["password"])) {
    $passwordErr = "Mot de passe attendu";
  } else {
    $password = test_input($_POST["password"]);
  }
    
  if (empty($_POST["passwordConfirm"])) {
    $passwordConfirmErr = "Mot de passe attendu";
  } else {
    $passwordConfirm = test_input($_POST["passwordConfirm"]);
  }

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
 