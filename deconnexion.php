<!--page de deconnexion-->
<?php
//On ferme la session
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Armada</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
   .responsive {
    width: 100%;
    height: auto;
}
  .carousel-inner img { 
      width: 100%; /* Set width to 100% */
      margin: auto;
  }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
     /* background-color: #2929a3 !important; */
    }
 .open .dropdown-toggle {
      color: #fff;
      background-color: #2929a3 !important;
  }
 .dropdown-menu li a {
      color: #2929a3 !important;
  }
  .dropdown-menu li a:hover {
      background-color: red !important;
  }  
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    .bg-1 { 
      background-color: #1abc9c;
      color: #ffffff;
      padding-top: 20px;
      padding-bottom: 20px;
  }

      .bg-2 { 
      background-color: #2929a3;
      color: #ffffff;
      padding: 25px;
    
  }
  .error {color: #FF0000;}
   /* Add a gray background color and some padding to the footer */
   footer {
       
       /*      background-color: #f2f2f2;*/
       /*      background-color: transparent;*/
                margin-bottom: 0;
             background-color: #2d2d30;
             border: 0;
             font-size: 11px !important;
             letter-spacing: 4px;
             opacity: 0.9;
             padding: 25px;
           }
  </style>
</head>
<body>


<div class="container-fluid bg-1 text-center">
  <h1>Amoureux des bateaux</h1>
  <h4>Bienvenue dans votre espace!</h4>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
    <ul class="nav navbar-nav">
      <li><a href="#">Accueil</a></li>
        <li><a href="#">Définir le role</a></li>
        <li><a href="#">Liste des bateaux</a></li>
        <li> <a href="#">Ajouter bateaux</a></li>
        <li> <a href="#">Modifier bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>

        </li>
        </ul>
    </div>
  </div>
</nav>

                 <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <div class="container text-center"> <h2>Vous êtes déconnecté. Vous serez dirigez vers le page d'accueil dans quelque instant.</h2> </div>
                </div>
                <script>
                        setTimeout(function (){
                            document.location.href = "index.php";
                        }, 3000);
                </script>
                 <?php
        // cette page php contient le pied de page de cette page php
        include 'pieds_pages.php';
        session_destroy();
        ?>