<?php
//On démarre la session
session_start();
//Si la session de cette personne existe, elle n'accède pas à cette page, on redirige vers sa page accueil 
if(isset($_SESSION["nom"]) )  
{
  
  //S'il s'agit d'un administrateur
  if($_SESSION["role"]=="Administrateur"){ 
    ?>
    <!-- Si l'utilisateur est administrateur, on le dirige vers la page administrateur-->
     <script> document.location.href = "accueil_admin.php";</script>
    <?php
  }
  //S'il s'agit d'un responsable
  if($_SESSION["role"]=="Responsable"){ 
    ?>
    <!-- Si l'utilisateur est administrateur, on le dirige vers la page administrateur-->
     <script> document.location.href = "accueil_respo.php";</script>
    <?php
  }
  //S'il s'agit d'un visiteur
  if($_SESSION["role"]=="Visiteur"){ 
    ?>
    <!-- Si l'utilisateur est administrateur, on le dirige vers la page administrateur-->
     <script> document.location.href = "accueil_visiteur.php";</script>
    <?php
  }
}
?>
<!--// pour éviter de refaire tout le temps le même haut de page, on la fait une fois ici, et on va l'inclure partout où on souhaitera-->
<!DOCTYPE html>
<html lang="en">
<head>
 
  <title>Armada</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style>
  /*
  .container {
      padding: 80px 120px;
      
  }
  */
  .person {
      border: 10px solid transparent;
      margin-bottom: 25px;
      width: 80%;
      height: 80%;
      opacity: 0.7;
  }
  .person:hover {
      border-color: #f1f1f1;
  }
  .responsive {
    width: 100%;
    height: auto;
}
  .carousel-inner img { 
      width: 100%; /* Set width to 100% */
      margin: auto;
  }

  .carousel-caption h3 {
      color: #fff !important;
  }
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; /* Hide the carousel text when the screen is less than 600 pixels wide */
    }
  }
  .bg-1 {
      background: #2d2d30;
      color: #bdbdbd;
  }
  .bg-1 h3 {color: #fff;}
  .bg-1 p {font-style: italic;}
  .list-group-item:first-child {
      border-top-right-radius: 0;
      border-top-left-radius: 0;
  }
  .list-group-item:last-child {
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
  }
  .thumbnail {
      padding: 0 0 15px 0;
      border: none;
      border-radius: 0;
  }
  .thumbnail p {
      margin-top: 15px;
      color: #555;
  }
  .btn {
      padding: 10px 20px;
      background-color: #333;
      color: #ffff80;
      border-radius: 0;
      transition: .2s;
  }
  .btn:hover, .btn:focus {
      border: 1px solid #2929a3;
      background-color: #fff;
      color: #000;
  }
  .modal-header, h4, .close {
      background-color: #2929a3;
      color: #fff !important;
      text-align: center;
      font-size: 30px;
  }
  .modal-header, .modal-body {
      padding: 40px 50px;
  }
  .nav-tabs li a {
      color: #777;
  }
      
.navbar {
      margin-bottom: 0;
      background-color: #2929a3;
      border: 0;
      font-size: 11px !important;
      letter-spacing: 4px;
      opacity: 0.9;
  }
  .navbar li a, .navbar .navbar-brand { 
      color: #fff !important;
  }
  .navbar-nav li a:hover {
      color: #fff !important;
  }
  .navbar-nav li.active a {
      color: #fff !important;
      background-color: #fff !important;
  }
  .navbar-default .navbar-toggle {
      border-color: transparent;
  }
  .open .dropdown-toggle {
      color: #fff;
      background-color: #555 !important;
  }
  .dropdown-menu li a {
      color: #000 !important;
  }
  .dropdown-menu li a:hover {
      background-color: red !important;
  }      
  .error {color: #FF0000;}
   /* Add a gray background color and some padding to the footer */
   footer {
       
       /*      background-color: #f2f2f2;*/
       /*      background-color: transparent;*/
                margin-bottom: 0;
             background-color: #2d2d30;
             border: 0;
             font-size: 11px !important;
             letter-spacing: 4px;
             opacity: 0.9;
             padding: 25px;
           }
  </style>

</head>
<body>
    
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">ARMADA</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="index.php">Accueil</a></li>
        <li><a href="liste_boat_non_inscrit.php">Consulter</a></li>
        <li><a href="connexion.php">Connexion</a></li>
        <li><a href="inscriptionform.php">S'Inscrire</a></li>
      </ul>
    </div>
  </div>
</nav>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="boat14.jpg" alt="Bateau Armada Mexico" class="responsive">
        <div class="carousel-caption">
          <h2>Bienvenue dans un site pour les fans de bateaux</h2>
          <h3>Apprendre divers choses de les bateaux!</h3>
        </div>      
      </div>

      <div class="item">
        <img src="boat2.jpg" alt="Boat7" class="responsive">
        <div class="carousel-caption">
          <h2>Bateaux de l'armada</h2>
          <h3>Apprendre caracteristiques de differentes bateaux!</h3>
        </div>      
      </div>
    
      <div class="item">
        <img src="boat3.jpg" alt="Boat3" class="responsive">
        <div class="carousel-caption">
          <h2>Rouen</h2>
          <h3>Venez nous rendre visite pour en savoir plus sur les bateaux.</h3>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>



