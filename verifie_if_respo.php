<?php
//On démarre la session
session_start();
//Si la session de cette n'existe pas et que quelqu'un essaie de le reconnecter avec l'url a cette page, on redirige
if(!isset($_SESSION["nom"]) )  
  header("Location: index.php");
  
  //Si un visiteur  veut accéder à cette page, il est redirigé
if($_SESSION["role"]=="Visiteur") 
{
  ?>
    <script>document.location.href = "accueil_visiteur.php";</script>
  <?php
}
?>