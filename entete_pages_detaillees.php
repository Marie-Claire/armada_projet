<?php
session_start();
//Si la session de cette n'existe pas et que quelqu'un essaie de le reconnecter avec l'url a cette page, on redirige
if(!isset($_SESSION["nom"]) )  
{
  ?>
    <script>document.location.href = "index.php";</script>
<?php
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Armada</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
   .responsive {
    width: 100%;
    height: auto;
}
  .carousel-inner img { 
      width: 100%; /* Set width to 100% */
      margin: auto;
  }
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
     /* background-color: #2929a3 !important; */
    }
 .open .dropdown-toggle {
      color: #fff;
      background-color: #2929a3 !important;
  }
 .dropdown-menu li a {
      color: #2929a3 !important;
  }
  .dropdown-menu li a:hover {
      background-color: red !important;
  }  
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    .bg-1 { 
      background-color: #1abc9c;
      color: #ffffff;
      padding-top: 20px;
      padding-bottom: 20px;
  }

      .bg-2 { 
      background-color: #2929a3;
      color: #ffffff;
      padding: 25px;
    
  }
  .error {color: #FF0000;}
   /* Add a gray background color and some padding to the footer */
   footer {
       
       /*      background-color: #f2f2f2;*/
       /*      background-color: transparent;*/
                margin-bottom: 0;
             background-color: #2d2d30;
             border: 0;
             font-size: 11px !important;
             letter-spacing: 4px;
             opacity: 0.9;
             padding: 25px;
           }
  </style>
</head>
<body>


<div class="container-fluid bg-1 text-center">
  <h1>Amoureux des bateaux</h1>
  <h4>Bienvenue dans votre espace!</h4>
</div>
