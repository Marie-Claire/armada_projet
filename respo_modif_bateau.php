<!--page liste des bateau du responsable, pour leur modification-->
<?php
//On démarre la session
session_start();
//Verifie si c'est un utilisateur
include 'verifie_if_respo.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>

<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $_SESSION["idBoat"]= $_POST["idboat"];
    ?>
    <script>document.location.href = "formboat_modif.php";</script>
    <?php
 
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
}
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li><a href="accueil_respo.php">Accueil</a></li>
      <li><a href="respo_bateaux_liste.php">Liste de bateaux</a></li>
        <li><a href="respo_ajout_bateau.php">Ajout de bateaux</a></li>
        <li class="active"><a href="#">Modification de bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>
<?php

$boats=Armada_GetBoats();
foreach($boats as $boat) {
          if($_SESSION["idAdm"]== $boat['idpers'])
          {
            $lienimg=$boat['imagebateau'];
            $lienpdf=$boat["pdfbateau"];
            $caracteristique=$boat["caracteristique"];
            $nombateau=$boat["nombateau"];
           ?>
 
<br><br><br>
     <div class="container text-center">
<div class="row">
   <div class="col-sm-4">
     <div class="thumbnail">
       <img src="<?php echo $lienimg?>" alt="B15" width="400" height="300">
       <br>
       <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">
       <input type="hidden" name="idboat" value="<?php echo $boat["idbateau"];?>" />
           <input type="submit" class="btn" name="submit" value="Modifier"> 
       </form>
     </div>
   </div>
   <div class="col-sm-8">
     <div class="thumbnail">
         <h1 class="text-center"><strong><?php echo $nombateau?></strong></h1>
         <h5 class="text-justify"><?php echo $caracteristique;?></h5>
     </div>
   </div>
 </div>
 </div>
           <?php
          }
        }
?>  

<?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>



