<!-- Page d'accueil de l'administrateur-->
<?php
//On démarre la session
session_start();

  //Verifie si la personne est un administrateur
  include 'verifie_if_admin.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li class="active"><a href="#">Accueil</a></li>
        <li><a href="administrateur.php">Définir le role</a></li>
        <li><a href="admin_liste_boats.php">Liste des bateaux</a></li>
        <li><a href="admin_ajout_boats.php">Ajouter bateaux</a></li>
        <li> <a href="admin_modif_boats.php">Modifier bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>

<!-- Images défilantes-->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="boat14.jpg" alt="Bateau Armada Mexico" class="responsive">
        <div class="carousel-caption">
          <h2>Bienvenue dans un site pour les fans de bateaux</h2>
          <h3>Apprendre divers choses de les bateaux!</h3>
        </div>      
      </div>

      <div class="item">
        <img src="boat2.jpg" alt="Boat7" class="responsive">
        <div class="carousel-caption">
          <h2>Bateaux de l'armada</h2>
          <h3>Apprendre caracteristiques de differentes bateaux!</h3>
        </div>      
      </div>
    
      <div class="item">
        <img src="boat3.jpg" alt="Boat3" class="responsive">
        <div class="carousel-caption">
          <h2>Rouen</h2>
          <h3>Venez nous rendre visite pour en savoir plus sur les bateaux.</h3>
        </div>      
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>

<!-- le contenu -->
<div class="container text-center">
  <h2>Bienvenue!</h2>
    <h3>Conoce los elementos que componen un barco.</h3>
  <p>Este es un sitio dedicado a los amantes de los barcos, en donde vamos a poder conocer diferentes cosas de ellos.</p> 
    <br><br>   
    <div class="row">
        <div class="col-sm-4">
          <span><img src="boat14.jpg" class="img-rounded" alt="boat14" width="304" height="236"></span>
            <h2><strong>Estribor</strong></h2>
            <p>Aqui podemos observar el costado derecho de un barco, también conocido como <em>estribor</em>. </p>
        </div>
        <div class="col-sm-4">
          <span><img src="boat12.jpg" class="img-rounded" alt="boat12" width="304" height="236"></span>
            <h2><strong>Babor</strong></h2>
            <p>Aqui podemos observar el costado izquierdo de un barco, también conocido como <em>estribor</em>. </p>
        </div>
        
        <div class="col-sm-4">
          <span><img src="boat15.jpg" class="img-rounded" alt="boat15" width="304" height="236"></span>
            <h2><strong>Cubierta</strong></h2>
            <p>En esta imagen podemos observar la <em>cubierta</em> de un barco, esta es la superficie principal del barco y pueden ser de metal o de madera. </p>
        </div>
        </div>
    <br><br>
    <div class="row">
        <div class="col-sm-4">
         <span><img src="boat8.jpg" class="img-rounded" alt="boat8" width="304" height="236"></span>
            <h2><strong>Popa</strong></h2>
            <p>La <em>popa</em> la parte posterior de una embarcación, en esta imagen podemos ver la bandera rusa en esa parte.</p>
        </div> 
         <div class="col-sm-4">
          <span><img src="boat6.jpg" class="img-rounded" alt="boat6" width="304" height="236"></span>
            <h2><strong>Mastil</strong></h2>
            <p>El <em>mastils</em> es un palo largo de una embarcación que sirve para sostener las velas.</p>
        </div>
        <div class="col-sm-4">
          <span><img src="boat10.jpg" class="img-rounded" alt="boat10" width="304" height="236"></span>
            <h2><strong>Proa</strong></h2>
            <p>En esta imagen podemos observar la <em>proa</em> de la embarcación, además de poder ver la inmensidad de sus velas.</p>
        </div>
    
    <br><br>
    </div>
</div>
<?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>