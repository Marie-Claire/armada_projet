<!--page d'attribution de rôle, page d l'administrateur-->
<?php
//On démarre la session
session_start();
  //Verifie si la personne est un administrateur
  include 'verifie_if_admin.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a href="accueil_admin.php">Accueil</a></li>
        <li class="active"><a href="#">Définir le role</a></li>
        <li><a href="admin_liste_boats.php">Liste des bateaux</a></li>
        <li><a href="admin_ajout_boats.php">Ajouter bateaux</a></li>
        <li> <a href="admin_modif_boats.php">Modifier bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>

    </body>

<?php
// define variables and set to empty values

$users = Armada_GetUtilisateurs();
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  //Mets à jour les roles
    $idpers = $_POST["id"];
    $roles = $_POST["roles"];
    $req=Armada_UpdateRole($idpers,$roles );
    $users = Armada_GetUtilisateurs();
}

if ($_SERVER["REQUEST_METHOD"] == "GET") {
  if ($_GET['ordre'] == "Ordre")
  {
      $users= Personne_alphabet();
 }
 else if($_GET['ordre'] == "Role")
 {
  $users= Personne_role();
 }
 else if($_GET['ordre'] == "Inscrit") $users = Armada_GetUtilisateurs();
 else $users = Armada_GetUtilisateurs();
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

  <!-- Pour ranger par role -->
  <form method="get" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"> 
  <br>
  Trier par :	<select name="ordre">
      <option value="Inscrit"> choisir </option>
      <option value="Inscrit"> Ordre d'inscription </option>
      <option value="Ordre">Ordre alphabetique</option>
      <option value="Role">Role</option>
      </select>

<input type="submit" value="Classer" />
  </form>
<br>

<div class="jumbotron">
  <div class="container">    
 <?php 
        foreach($users as $user) {
          if($_SESSION["idAdm"]!=$user['idpers'])
          {
  ?>         

              <p> <?php echo $user['nompers'].' '.$user['prenompers'] ?>  </p>    
    
    <!--Ce formulaire contient les utilisateur et leur role-->  
    <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Role:
  <input type="hidden" name="id" value="<?php echo $user['idpers'];?>" />
  <input type="radio" name="roles" <?php if (isset($user['rolepers']) && $user['rolepers']=="Visiteur") echo "checked";?> value="Visiteur">Visiteur
  <input type="radio" name="roles" <?php if (isset($user['rolepers']) && $user['rolepers']=="Responsable") echo "checked";?> value="Responsable">Responsable
  <input type="radio" name="roles" <?php if (isset($user['rolepers']) && $user['rolepers']=="Administrateur") echo "checked";?> value="Administrateur">Administrateur
  <br><br>
  <input type="submit" name="submit" value="Submit">  
  </form>
     <br>

          <?php
          }
          
        }

  ?>
    
  </div>
</div>
<?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>
 