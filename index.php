<!--Page d'accueil-->
<?php
//On inclut l'entete ici
    include 'entete_pages.php';
?>

<div class="container text-center">
  <h2>Bienvenue!</h2>
    <h3>Conoce los elementos que componen un barco.</h3>
  <p>Este es un sitio dedicado a los amantes de los barcos, en donde vamos a poder conocer diferentes cosas de ellos.</p> 
    <br><br>   
    <div class="row">
        <div class="col-sm-4">
          <span><img src="boat14.jpg" class="img-rounded" alt="boat14" width="304" height="236"></span>
            <h2><strong>Estribor</strong></h2>
            <p>Aqui podemos observar el costado derecho de un barco, también conocido como <em>estribor</em>. </p>
        </div>
        <div class="col-sm-4">
          <span><img src="boat12.jpg" class="img-rounded" alt="boat12" width="304" height="236"></span>
            <h2><strong>Babor</strong></h2>
            <p>Aqui podemos observar el costado izquierdo de un barco, también conocido como <em>estribor</em>. </p>
        </div>
        
        <div class="col-sm-4">
          <span><img src="boat15.jpg" class="img-rounded" alt="boat15" width="304" height="236"></span>
            <h2><strong>Cubierta</strong></h2>
            <p>En esta imagen podemos observar la <em>cubierta</em> de un barco, esta es la superficie principal del barco y pueden ser de metal o de madera. </p>
        </div>
        </div>
    <br><br>
    <div class="row">
        <div class="col-sm-4">
         <span><img src="boat8.jpg" class="img-rounded" alt="boat8" width="304" height="236"></span>
            <h2><strong>Popa</strong></h2>
            <p>La <em>popa</em> la parte posterior de una embarcación, en esta imagen podemos ver la bandera rusa en esa parte.</p>
        </div> 
         <div class="col-sm-4">
          <span><img src="boat6.jpg" class="img-rounded" alt="boat6" width="304" height="236"></span>
            <h2><strong>Mastil</strong></h2>
            <p>El <em>mastils</em> es un palo largo de una embarcación que sirve para sostener las velas.</p>
        </div>
        <div class="col-sm-4">
          <span><img src="boat10.jpg" class="img-rounded" alt="boat10" width="304" height="236"></span>
            <h2><strong>Proa</strong></h2>
            <p>En esta imagen podemos observar la <em>proa</em> de la embarcación, además de poder ver la inmensidad de sus velas.</p>
        </div>
    
    <br><br>
    </div>
</div>
<?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>