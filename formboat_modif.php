<!-- page de modification d'un bateau, page du responsable-->
<?php
//On démarre la session
session_start();
//Verifie si c'est un utilisateur
include 'verifie_if_respo.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li><a href="accueil_respo.php">Accueil</a></li>
      <li><a href="respo_bateaux_liste.php">Liste de bateaux</a></li>
        <li><a href="respo_ajout_bateau.php">Ajout de bateaux</a></li>
        <li class="active"><a href="respo_modif_bateau.php">Modification de bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>
<?php
$boats=Armada_GetBoats();
//if ($users == null) {exit();}
foreach($boats as $boat) {
          if( $_SESSION["idBoat"]== $boat['idbateau'])
          {
            $lienimg=$boat['imagebateau'];
            $lienpdf=$boat["pdfbateau"];
            $caracteristique=$boat["caracteristique"];
            $nombateau=$boat["nombateau"];
            $detut=$boat["debutbateau"];
            $fin=$boat["finbateau"];
            }
            }
            ?>
<?php

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
if($_POST["modifier"])
{
//On vérifie les entrées
// Si il y'a une erreur, alors 
if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert de la photo";
if ($_FILES['fichier']['error'] > 0) $erreur = "Erreur lors du transfert du fichier";

$extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png');
$extensions_pdf_valides = array('pdf');
//1. strrchr renvoie l'extension avec le point (« . »).
//2. substr(chaine,1) ignore le premier caractère de chaine.
//3. strtolower met l'extension en minuscules.
$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
$extension_pdf_upload = strtolower(  substr(  strrchr($_FILES['fichier']['name'], '.')  ,1)  );

//si c'est une image on upload la photo
if ( in_array($extension_upload,$extensions_valides) OR empty($_FILES['image']['name'])) { 
    //si le fichier est trop grand
    if(empty($_FILES['image']['name']))
    {
    $nomimage=$lienimg;
    $resultatimg =1;
    }
    else 
    {$nomimage = "imageupload/img{$_SESSION['nom']}.{$_FILES['image']['name']}";
    $resultatimg = move_uploaded_file($_FILES['image']['tmp_name'],$nomimage);
    }
} 
else $imageErr ="* Vérifiez l'extension";

//Si c'est un fichier on l'upload
if ( in_array($extension_pdf_upload,$extensions_pdf_valides) OR empty($_FILES['fichier']['name']) ) {
    //si le fichier est trop grand
    if(empty($_FILES['fichier']['name']))
    {
        $nomfichier=$lienpdf;
        $resultatfile =1;
    }
    else 
    {$nomfichier = "pdfupload/pdf{$_SESSION['nom']}.{$_FILES['fichier']['name']}";
    $resultatfile = move_uploaded_file($_FILES['fichier']['tmp_name'],$nomfichier);
   }
}
else $pdfErr ="* Vérifiez l'extension";

if (empty($_POST["comment"])) {
$commentErr = "* Commentaire attendu";
} else {
$comment = $_POST["comment"];
}

if (empty($_POST["nomboat"])) {
$nomboatErr = "* Nom attendu";
} 
else if (!preg_match("/^[a-zA-Z ]*$/",$_POST["nomboat"])) {
    $nomboatErr = "Entrer uniquement des lettres ou des espaces"; 
}
else {
$nomboat= $_POST["nomboat"];
// Vérifie si le nom contient unique des lettres et des espacesc
}
$debut=$_POST["debut"];
$fin=$_POST["fin"];
//Si la copie de l'image et du fichier a été faite
if ( ($resultatimg && $resultatfile) )  {
    //Si l'image ou le pdf n'est pas inchangé
    if(empty($pdfErr) && empty($imageErr) && empty($nomboatErr) && empty($commentErr) ){
        $request = Armada_UpdateBoat($comment, $nomimage, $nomfichier, $debut, $fin, $nomboat);
        if($request){
          $resutat_transaction = "Vos entrées sont en traitement...";
          ?>
        <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong> <?php echo $resutat_transaction?>.</strong> 
        </div>
        <?php
        $_SESSION["commentaire"]=$comment;
        $_SESSION["momimage"]=$nomimage;
        $_SESSION["momfichier"]=$nomfichier;
        $_SESSION["debutboat"]=$debut;
        $_SESSION["finboat"]=$fin;
        $_SESSION["momdubateau"]=$nomboat;
        
        ?>
          <script>
        setTimeout(function (){
          document.location.href = "formboat_modif.php";
                              }, 1000);
        </script>
        <?php
      }
        } 
else{
    ?>
    <div class="alert alert-danger alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong> Vérifiez vos entrées></strong> 
  </div>
  
    <?php
    ;
}
}else{
    ?>
    <div class="alert alert-danger alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong> Image ou pdf trop grand, ou extensions non valides.</strong> 
  </div>
  
    <?php
  }
}
    if($_POST["supprimer"])
    {
        $request = Armada_DeleteBoat();
        if($request){
            $resutat_transaction = "Bateau supprimé";
    ?>
  <div class="alert alert-success alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong> <?php echo $resutat_transaction?>.</strong> 
  </div>
  <script>
  setTimeout(function (){
    document.location.href = "respo_modif_bateau.php";
                        }, 1000);
  </script>
            <?php
        
        }
    }
function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
}
?>
 
            <br><br><br>
    <div class="container text-justify">
         <div >
               <div class="col-sm-7">
                 <div class="thumbnail">
                   <img src="<?php echo $lienimg?>" alt="B15" width="400" height="300">
                   <br>
                   <a href="<?php echo $lienpdf?>" download>Télécharger le PDF pour plus d'information.</a>
                 </div>
                 <div class="thumbnail">
                     <h1 class="text-center"><strong><?php echo $nombateau?></strong></h1>
                     <h5 class="text-center"><?php echo $caracteristique;?></h5>
                 </div>
               </div>

<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">  
  Nom du bateau :<br><input type="text" name="nomboat" value="<?php echo $nombateau; ?>" required>
  <br> <span class="error"> <?php echo $nomboatErr;?></span>
  <br>
  Caractéristiques : <br>
  <textarea rows="5" cols="40" name="comment" required><?php echo $caracteristique; ?></textarea>
  <br> <span class="error"> <?php echo $commentErr;?></span>
  <br>
  <label  for="image">Image</label>
  <input  type="file" id="image" name="image" > 
   <span class="error"> <?php echo $imageErr;?></span>
  <br>
  <label  for="fichier">PDF</label>
  <input type="file" id="fichier" name="fichier" >
   <span class="error"> <?php echo $pdfErr;?></span>
<br>
    <label ><strong>Début: </strong></label>
     <input  type="date" id="debut" name="debut" value="<?php echo $detut; ?>" required>
      
     <label ><strong>   Fin:</strong></label>
   <input  type="date" id="fin" name="fin" value="<?php echo $fin; ?>" required>  
   <br> <span class="error"> <?php echo $dateErr;?></span>  
      <br>
  <input type="submit" name="modifier" value="Modifier">  
     Ou bien
  <input type="submit" name="supprimer" value="Supprimer"> 
  <br><br>
</form>
    </div>
    

</div>
             
                       <?php
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>



