<!--//Cette page php est celle qui se charge des vérifications et tests du formulaire de connexion-->

<?php

//La connection à la base de donnée pour l'insertion des données
    include 'Testconnexionbd.php';

// define variables and set to empty values
$emailErr = $passwordErr = "";
$email = $password =  "";
$inscrit = "non";
if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if (empty($_POST["email"])) {
    $emailErr = "Email attendu";
  } else {
    $email = test_input($_POST["email"]);
    // on vérifie que l'email entré a un format valide
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Format d'email invalide"; 
    }
     
  }
    
  if (empty($_POST["password"])) {
    $passwordErr = "Mot de passe attendu";
  } else {
    $password = test_input($_POST["password"]);
  }

}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}



    //S'il n'ya aucunes erreurs repertoriées
if ( empty($emailErr) && empty($passwordErr))
{
    //Si les cases ne sont pas vides
    if ( !empty($email)  && !empty($password))
    { 
      $users = Armada_GetUtilisateurs();
      //if ($users == null) {exit();}
      foreach($users as $user) {
              //Si l'adresse mail et le mot de passe existent déjà
            if($email==$user['emailpers'] && sha1($password)==$user['passwordpers'] ) 
            {
              //Si l'utilisateur est inscrit on met dans $inscrit = "oui"
              $inscrit = "oui";
              $_SESSION["nom"]= $user['nompers'];
              $_SESSION["prenom"]=$user['prenompers'];
              $_SESSION["email"]=$user['emailpers'];
              $_SESSION["password"]=$user['passwordpers'];
                if($user['rolepers']=="Visiteur") $_SESSION["role"]= 'Visiteur';
                if($user['rolepers']=="Responsable") $_SESSION["role"]= 'Responsable';
                if($user['rolepers']=="Administrateur") $_SESSION["role"]= 'Administrateur';
                $_SESSION["idAdm"]=$user['idpers'];
                $inscriptionconfirm = 'Bienvenue'.' '.$nom.' '.$prenom;
                
                ?>
                 <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Bienvenue <?php echo $_SESSION["nom"].' '.$_SESSION["prenom"]?>.</strong> 
                </div>
               <?php
               if($user['rolepers']=="Administrateur")
               {
                header("Location: accueil_admin.php");
                ?>
               <script>
               // Si l'utilisateur est administrateur, on le dirige vers la page administrateur
                    setTimeout(function (){
                     document.location.href = "accueil_admin.php";
                        }, 2000);
                        
                </script>
                <?php ;
                }
                else if($user['rolepers']=="Visiteur")
                {
                  header("Location: accueil_visiteur.php");
                  ?>
               <script>
               // Si l'utilisateur est administrateur, on le dirige vers la page administrateur
               setTimeout(function (){
                 document.location.href = "accueil_visiteur.php";
                        }, 2000);
                            
                </script>
                <?php ;
                }
                else {
                  header("Location: accueil_respo.php");
                  ?>
               <script>
               // Si l'utilisateur est administrateur, on le dirige vers la page administrateur
               setTimeout(function (){
                 document.location.href = "accueil_respo.php";
                        }, 2000);
                </script>
                <?php ;
                }
                
              }   
         } 
         if($inscrit=="non")
         {
           ?>
              <div class="alert alert-danger alert-dismissible">
             <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             <strong>Adresse e-mail et mot de passe non correct.</strong> 
             </div>
             <script>
                    setTimeOut(3000);
             </script>
           <?php
         }
    }    
}
?>
