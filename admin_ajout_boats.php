<!--page d'ajout de bateaux pour l'administrateur-->
<?php
//On démarre la session
session_start();

  //Verifie si la personne est un administrateur
  include 'verifie_if_admin.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li><a href="accueil_admin.php">Accueil</a></li>
        <li><a href="administrateur.php">Définir le role</a></li>
        <li><a href="admin_liste_boats.php">Liste des bateaux</a></li>
        <li class="active"> <a href="#">Ajouter bateaux</a></li>
        <li> <a href="admin_modif_boats.php">Modifier bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>

<?php
    $comment=$nomboat=$debut=$fin=$_SESSION["momimage1"]=$extension_upload =$extension_pdf_upload="";
$nomimage = $resultatimg = $_SESSION["momfichier1"]= $nomfichier = $resultatfile ="";
$pdfErr =$commentErr =$nomboatErr=$imageErr="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  // Si il y'a une erreur, alors 
  if ($_FILES['image']['error'] > 0) $erreur = "Erreur lors du transfert de la photo";
  if ($_FILES['fichier']['error'] > 0) $erreur = "Erreur lors du transfert du fichier";

  $extensions_valides = array( 'jpg' , 'jpeg' , 'gif' , 'png');
  $extensions_pdf_valides = array('pdf');
  //1. strrchr renvoie l'extension avec le point (« . »).
//2. substr(chaine,1) ignore le premier caractère de chaine.
//3. strtolower met l'extension en minuscules.
$extension_upload = strtolower(  substr(  strrchr($_FILES['image']['name'], '.')  ,1)  );
$extension_pdf_upload = strtolower(  substr(  strrchr($_FILES['fichier']['name'], '.')  ,1)  );

//si c'est une image on upload la photo
if ( in_array($extension_upload,$extensions_valides) ) { 
  $nomimage = "imageupload/img{$_SESSION['nom']}.{$_FILES['image']['name']}";
  $resultatimg = move_uploaded_file($_FILES['image']['tmp_name'],$nomimage);
} 
else $imageErr ="* Vérifiez l'extension";

//Si c'est un fichier on l'upload
if ( in_array($extension_pdf_upload,$extensions_pdf_valides) ) {
  $nomfichier = "pdfupload/pdf{$_SESSION['nom']}.{$_FILES['fichier']['name']}";
  $resultatfile = move_uploaded_file($_FILES['fichier']['tmp_name'],$nomfichier);
}
else $pdfErr ="* Vérifiez l'extension";

if (empty($_POST["comment"])) {
  $commentErr = "* Commentaire attendu";
} else {
  $comment = $_POST["comment"];
}

if (empty($_POST["nomboat"])) {
  $nomboatErr = "* Nom attendu";
} else {
  $nomboat= $_POST["nomboat"];
  // Vérifie si le nom contient unique des lettres et des espacesc
  if (!preg_match("/^[a-zA-Z ]*$/",$nomboat)) {
    $nomboatErr = "Entrer uniquement des lettres ou des espaces"; 
  }
}
$debut=$_POST["debut"];
$fin=$_POST["fin"];
    //Si la copie de l'image et du fichier a été faite
  if ($resultatimg && $resultatfile ) {
    $request = Armada_SetBoat($comment, $nomimage, $nomfichier, $debut, $fin, $nomboat);
    if($request){
      $resutat_transaction = "Vos entrées ont été enregistrées";
      ?>
    <div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> <?php echo $resutat_transaction?>.</strong> 
    </div>
    <?php
    $_SESSION["commentaire"]=$comment;
    $_SESSION["momimage"]=$nomimage;
    $_SESSION["momfichier"]=$nomfichier;
    $_SESSION["debut"]=$debut;
    $_SESSION["fin"]=$fin;
    $_SESSION["momdubateau"]=$nomboat;
    ;
  }
    else{
      ?>
      <div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong> Vérifiez vos entrées, les images et pdf sont trop grands</strong> 
    </div>
    
      <?php
      ;
    } 
  }else{
    ?>
    <div class="alert alert-danger alert-dismissible">
  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong> Vérifiez vos entrées, les images ou pdf sont trop grands</strong> 
  </div>
  
    <?php
  }
  


function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
}
?>

<div class="container text-align">
<br><br>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" enctype="multipart/form-data">  
  Nom du bateau :<br><input type="text" name="nomboat" value="<?php echo $nomboat; ?>" required>
  <br> <span class="error"> <?php echo $nomboatErr;?></span>
  <br><br>
  Caractéristiques : <br>
  <textarea rows="5" cols="40" name="comment" required><?php echo $comment;?></textarea>
  <br> <span class="error"> <?php echo $commentErr;?></span>
  <br><br>
  <label for="image">Image</label>
  <input class="container text-center" type="file" id="image" name="image" required> 
  <br> <span class="error"> <?php echo $imageErr;?></span>
  <br><br><br>
  <label for="fichier">PDF - Taille max 3,4 Mo</label>
  <input class="container text-center" type="file" id="fichier" name="fichier" required>
  <br> <span class="error"> <?php echo $pdfErr;?></span> <br><br>
    <label ><strong>Début: </strong></label>
     <input class="text-center" type="date" id="debut" name="debut" value="<?php echo $debut?>" required>
      
      <label ><strong>    Fin : </strong></label>
   <input class="text-center" type="date" id="fin" name="fin" value="<?php echo $fin?>" required>  
   <br> <span class="error"> <?php echo $dateErr;?></span>  
      <br><br>
  <input type="submit" name="submit" value="Submit">  
  <br><br>
</form>
</div>
<?php 
// cette page php contient le pied de page de cette page php
include 'pieds_pages.php';
?>