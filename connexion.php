<!-- Page de connexion -->
<?php
//On démarre la session
session_start();

  // cette page php contient l'entete de cette page php
    include 'entete_pages.php';
// cette page php traite les vérifications liées à l'inscription
    include 'connexion_prise_charge.php';
?>
<div class="container">

<h2>Connexion</h2>
<p><span class="error">* information attendue. <?php echo urldecode($_GET["message"]) ?></span></p>
<form  method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  <p><span class="glyphicon glyphicon-user"></span>  Idendifiant &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="email" name="email" value="<?php echo $email; ?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  <span class="glyphicon glyphicon-cog"></span>  Mot de passe  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="password" value="<?php echo $password; ?>">
  <span class="error">* <?php echo $passwordErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Soumettre">  <br><br><br><br>
</form>

</div>

        <?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>
