<?php
//On démarre la session
session_start();
//Si la session de cette n'existe pas et que quelqu'un essaie de le reconnecter avec l'url a cette page, on redirige
if(!isset($_SESSION["nom"]) )  
{
  ?>
    <script>document.location.href = "index.php";</script>
<?php
}

//Si un visiteur  veut accéder à cette page, iest redirigé
if($_SESSION["role"]=="Visiteur") 
{
  ?>
    <script>document.location.href = "accueil_visiteur.php";</script>
  <?php
}
  //Si un Responsable  veut accéder à cette page, il est redirigé
  if($_SESSION["role"]=="Responsable")
  { 
   ?>
   <script>document.location.href = "accueil_respo.php";</script>
   <?php
  }
?>