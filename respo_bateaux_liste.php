<!-- Page d'info détaillée pour le responsable de bateau-->
<?php
//On démarre la session
session_start();
//Verifie si c'est un utilisateur
include 'verifie_if_respo.php';
//Elle contient l'entete de cette page
include 'Testconnexionbd.php';
//Elle contient l'entete de cette page
include 'entete_pages_detaillees.php';
?>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Armada</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
      <li><a href="accueil_respo.php">Accueil</a></li>
      <li class="active"><a href="#">Liste de bateaux</a></li>
        <li><a href="respo_ajout_bateau.php">Ajout de bateaux</a></li>
        <li><a href="respo_modif_bateau.php">Modification de bateaux</a></li>
      </ul>
        
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Profils
          <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#"><?php echo $_SESSION["nom"].' '.$_SESSION['prenom']?></a></li>
            <li><a href="#"><?php echo $_SESSION["role"]?></a></li> 
              <li><a href="deconnexion.php"><span class="glyphicon glyphicon-log-in"></span> Se déconnecter</a></li>
          </ul>
        </li>
        </ul>
    </div>
  </div>
</nav>
<?php
$boats=Armada_GetBoats();
foreach($boats as $boat) {
         
            $lienimg=$boat['imagebateau'];
            $lienpdf=$boat["pdfbateau"];
            $caracteristique=$boat["caracteristique"];
            $nombateau=$boat["nombateau"];
           ?>
 
<br><br><br>
     <div class="container text-center">
<div class="row">
   <div class="col-sm-4">
     <div class="thumbnail">
       <img src="<?php echo $lienimg?>" alt="B15" width="400" height="300">
       <br>
       <a href="<?php echo $lienpdf?>" download>Télécharger PDF pour plus d'information.</a>
     </div>
   </div>
   <div class="col-sm-8">
     <div class="thumbnail">
         <h1 class="text-center"><strong><?php echo $nombateau?></strong></h1>
         <h5 class="text-center"><?php echo $caracteristique;?></h5><br>
     </div>
   </div>
 </div>
 </div>
           <?php
          
        }
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>



