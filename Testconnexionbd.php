
<?php
session_start();
//Connection à la base de donnée
function Armada_Connection()
{
    /* 
    //enligne
    $host = 'localhost';
    $username = 'grp_7_4';
    $password = 'eif3Do6ail';
    $dbname = 'bdd_7_4';
    */
    //en local
    $host = 'localhost';
    $username = 'root';
    $password = 'root';
    $dbname = 'bdd_7_4';
    
    $pdo [PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;

    try{

        $con = new PDO("mysql:host=".$host.";dbname=".$dbname, $username, $password, $pdo);
        return $con;
    }
    catch(Exception $e){
        echo "Erreur lors de la connexion à la base de données: ".$e->getMessage();
    }
    
}

//Recupère toutes les personnes inscrites
function Armada_GetUtilisateurs()
{
    $con = Armada_Connection();
    $sql = 'SELECT * FROM personnes'; 
    $query  = $con -> query($sql); 
    
    $array = [];
    while($result = $query -> fetch()) {
        $array[] = $result;
    }

    if(sizeof($array) > 0){
        return $array;
    }
    else{
        return null;
    }
}

//Entre les informations dans la base de données sur Personnes
function Armada_SetUtilisateurs($nom, $prenom, $email, $password)
{
    $con = Armada_Connection();
    $sql = "INSERT INTO personnes (nompers, prenompers, emailpers, passwordpers, rolepers) VALUES (?, ?, ?, ?, 'Visiteur')"; 
    $query = $con -> prepare($sql);
    $query -> execute(array($nom, $prenom, $email, sha1($password))); 
    
    mysqli_close($con);
    //echo $query;
    return $query;
}

//On modifie le role dans la base de données
function Armada_UpdateRole($id, $role)
{
    $con = Armada_Connection();
    $sql = "UPDATE personnes SET rolepers =? WHERE idpers =? " ; 
    
    $query = $con -> prepare($sql);
    $query -> execute(array($role, $id)); 
    
    mysqli_close($con);
    //echo $query;
    return $query;
}

//Cette fonction range par ordre alphabétique
function Personne_alphabet()
{
    $con = Armada_Connection();
    $sql = "SELECT * FROM personnes ORDER BY nompers , prenompers " ;
    $query  = $con -> query($sql); 
    
    $array = [];
    while($result = $query -> fetch()) {
        $array[] = $result;
    }

    if(sizeof($array) > 0){
        return $array;
    }
    else{
        return null;
    }
}
//Cette fonction range par role 
function Personne_role()
{
    $con = Armada_Connection();
    $sql = "SELECT * FROM personnes ORDER BY rolepers" ;
    $query  = $con -> query($sql); 
    
    $array = [];
    while($result = $query -> fetch()) {
        $array[] = $result;
    }

    if(sizeof($array) > 0){
        return $array;
    }
    else{
        return null;
    }
}


//Entre les informations dans la base de données sur Bateaux
function Armada_SetBoat($comment, $nomimage, $nomfichier, $debut, $fin, $nombateau)
{
    $idpers=$_SESSION["idAdm"];
    $con = Armada_Connection();
    $sql = "INSERT INTO bateaux (nombateau, caracteristique, idpers, imagebateau, pdfbateau, debutbateau, finbateau) VALUES (?, ?, ?, ?, ?, ?, ?)"; 

    $query = $con -> prepare($sql);
    $query -> execute(array($nombateau, $comment, $idpers, $nomimage, $nomfichier, $debut, $fin)); 
    
    mysqli_close($con);
    //echo $query;
    return $query;
}

//Recupere les informations dans la base de données sur Bateaux
function Armada_GetBoats()
{
    $con = Armada_Connection();
    $sql = 'SELECT * FROM bateaux ORDER BY nombateau' ;
    $query  = $con -> query($sql); 
    
    $array = [];
    while($result = $query -> fetch()) {
        $array[] = $result;
    }
    
    if(sizeof($array) > 0){
        return $array;
    }
    else{
        return null;
    }

}
//Update les informations dans la base de données sur Bateaux
function Armada_UpdateBoat($comment, $nomimage, $nomfichier, $debut, $fin, $nomboat)
{
    $con = Armada_Connection();
    $identifiant=$_SESSION["idBoat"];
    //Si toutes les cases ne sont pas vides
    if(!empty($nomimage) && !empty($nomfichier) )
    {
        $sql = "UPDATE bateaux SET nombateau =?, caracteristique =?, imagebateau =?, pdfbateau=?, debutbateau=?, finbateau=? WHERE idbateau =?" ; 
        $query = $con -> prepare($sql);
        $query -> execute(array($nomboat, $comment, $nomimage, $nomfichier, $debut, $fin, $identifiant)); 
    }
    //Si il n'ya pas une  image
    else  if(empty($nomimage) && !empty($nomfichier) )
    {
        $sql = "UPDATE bateaux SET nombateau =?, caracteristique =?, pdfbateau=?, debutbateau=?, finbateau=? WHERE idbateau =?" ; 
        $query = $con -> prepare($sql);
        $query -> execute(array($nomboat, $comment, $nomfichier, $debut, $fin, $identifiant)); 
    }
    //Si il n'ya pas un fichier
    else if(!empty($nomimage) && empty($nomfichier) )
    {
        $sql = "UPDATE bateaux SET nombateau =?, caracteristique =?, imagebateau =?, debutbateau=?, finbateau=? WHERE idbateau =?" ; 
        $query = $con -> prepare($sql);
        $query -> execute(array($nomboat, $comment, $nomimage, $debut, $fin, $identifiant)); 
    }
    //Si il n'ya pas un fichier et image
    else if(empty($nomimage) && empty($nomfichier) )
    {
        $sql = "UPDATE bateaux SET nombateau =?, caracteristique =?, debutbateau=?, finbateau=? WHERE idbateau =?" ; 
        $query = $con -> prepare($sql);
        $query -> execute(array($nomboat, $comment, $debut, $fin, $identifiant)); 
    }

    mysqli_close($con);
    //echo $query;
    return $query;
}

//Update les informations dans la base de données sur Bateaux
function Armada_DeleteBoat()
{
    $con = Armada_Connection();
    $identifiant=$_SESSION["idBoat"];
    $sql = "DELETE FROM bateaux WHERE idbateau =? " ; 
    $query = $con -> prepare($sql);
    $query -> execute(array($identifiant)); 

    mysqli_close($con);
    
    return $query;
}
?>
