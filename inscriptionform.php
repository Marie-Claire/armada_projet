<!-- Page d'incription-->

<?php
//On démarre la session
session_start();

// cette page php contient l'entete de cette page php
    include 'entete_pages.php';
// cette page php traite les vérifications liées à l'inscription
    include 'inscription_prise_charge.php';
//La connection à la base de donnée pour l'insertion des données
    include 'Testconnexionbd.php';
?>

<div class="container text-justify">

<h2>Inscription</h2>
<p><span class="error" >* information attendue</span></p>
<form  method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
 <span class="glyphicon glyphicon-user"></span> Nom &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <input type="text" name="nom" value="<?php echo $nom; ?>">
       <span class="error">* <?php echo $nomErr;?></span>
  <br><br>
  <span class="glyphicon glyphicon-user"></span> Prenom &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="prenom" value="<?php echo $prenom; ?>">
  <span class="error">* <?php echo $prenomErr;?></span>
  <br><br>
  <span class="glyphicon glyphicon-envelope"></span> E-mail &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="email" name="email" value="<?php echo $email; ?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  <span class="glyphicon glyphicon-cog"></span> Mot de passe  &nbsp;&nbsp;<input type="password" name="password" value="<?php echo $password; ?>">
  <span class="error">* <?php echo $passwordErr;?></span>
  <br><br>
   <span class="glyphicon glyphicon-lock"></span> Confirmer  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="password" name="passwordConfirm" value="<?php echo $passwordConfirm; ?>">
  <span class="error">* <?php echo $passwordConfirmErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Soumettre">  <br><br><br><br>
</form>
</div>

<?php

//Si les cases ne sont pas vides
if ( !empty($nom) && !empty($prenom)  && !empty($email)  && !empty($password) && !empty($passwordConfirm))
{ 
    // On se rassure que les mots de passes correspondent
    if ($passwordConfirm != $password){
        $alertbox = "Les mots de passe ne sont pas identiques";
    }
    else{     
        $users = Armada_GetUtilisateurs();
        //if ($users == null) {exit();}
        foreach($users as $user) {
            //Si le nom et le prenom existe déjà
            if($nom==$user['nompers'] && $prenom==$user['prenompers'] ) 
            { $nom_prenom='Ce nom et prenom existent déjà ' ; }
            //Si l'adresse mail existe déjà
            if($email==$user['emailpers']) 
            {
                $emailExistErr=' Cet email existe déjà';
            }
            
        }

        //S'il n'ya aucunes erreurs repertoriées
       if ( empty($nomErr) && empty($prenomErr)  && empty($emailErr)  && empty($passwordErr) && empty($passwordConfirmErr) && empty($nom_prenom) && empty($emailExistErr))
       {
        //On introduit la personne dans la base de donnée
        $query = Armada_SetUtilisateurs($nom, $prenom, $email, $password); 
        if ($query) {
            $inscriptionconfirm = "Vous êtes bien incrit. Entrez votre e-mail et votre mot de passe.";
           
            //On crée les variables de session dans $_SESSION
             $_SESSION["nom"]= $nom;
              $_SESSION["prenom"]=$prenom;
              $_SESSION["email"]=$email;
              $_SESSION["password"]=$password;
              $_SESSION["role"]= 'Visiteur';
            ?> 
                <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Vous êtes bien incrit <?php echo $_SESSION["nom"].' '.$_SESSION["prenom"]?>.</strong> 
                </div>
                <script>
                        setTimeout(function (){
                            document.location.href = "accueil_visiteur.php";
                        }, 2000);
                </script>
            <?php   
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($con);
        }
       }  
     } 
       
    }    
?>
<?php
if ( !empty($alertbox) OR !empty($nom_prenom) OR !empty($emailExistErr)) :
?>
 <div class="alert alert-danger alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong><?php echo $alertbox.$nom_prenom.$emailExistErr?></strong> 
  </div>
<?php
 endif;
?>

        <?php 
        // cette page php contient le pied de page de cette page php
             include 'pieds_pages.php';
        ?>
    

